import { Injectable } from '@angular/core'; // function อื่น สามารถเรียกใช้งาน
import { Observable } from 'rxjs'; // check ข้อมูลที่ใส่มา ให้เก็บข้อมูลให้ถูกต้อง
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenserviceService } from '../services/authenservice.service';

@Injectable()
export class AuthenGuard implements CanActivate {
  constructor(private router: Router, private authService: AuthenserviceService) {}

  canActivate(
    // function check login ?
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.authService.checklogin()) {
      this.router.navigate(['/login']); // back to login if not authenticated
      return false;
    }
    return true;
  }
}
