import { Injectable } from '@angular/core';
import { observable, Observable } from 'rxjs';
import * as io from 'socket.io-client'; // * import ทุกที่
import { TokenserviceService } from './tokenservice.service';
@Injectable({
  providedIn: 'root'
})
export class ChatserviceService {
  mmsgbox: string[] = []; // msg box เปล่าๆ
  roomMapMsg: any = []; // name ที่ map กับ msg box
  onlineuser: any;
  socket = io.connect('http://localhost:4500/'); // สร้าง connection
  constructor(private tokenService: TokenserviceService) {}

  setupconnect() {
    console.log('connectedtoserver');
    this.socket.on('testConnect', (data) => {
      console.log(data);
    });

    var email = this.tokenService.getuser(); // get user from tokenserviceservice
    this.socket.emit('testSendUser', email); // test send datauser to server

    this.socket.on('dataonlineuser', (dataonlineuser) => {
      console.log(dataonlineuser);
      this.onlineuser = dataonlineuser;
    });
  }
  getonlineuser() {
    var myuser = this.onlineuser.findIndex((obj) => {
      return obj.displayname == this.tokenService.getuser().displayname;
    });
    if (myuser !== -1) {
      this.onlineuser.splice(myuser, 1);
    }
    return this.onlineuser;
  }

  createRoom() {
    var tempOnline = this.onlineuser;
    var roomList = []; // findIndex ใช้กับ array เท่านั้น
    var index = tempOnline.findIndex((obj) => {
      // onlineuser ทั้งหมด ยกเว้นตัวเอง
      return obj.email == this.tokenService.getuser().email; // เอา email ไปหา
    });
    if (index !== -1) {
      tempOnline.splice(index, 1);
    }
    // console.log(tempOnline);
    //console.log(this.tokenService.getuser().email);
    for (let i = 0; i < tempOnline.length; i++) {
      var tempRoom = ''; // tempRoom ชั่วคราว
      if (this.tokenService.getuser().email > tempOnline[i].email) {
        tempRoom = this.tokenService.getuser().email + '*' + tempOnline[i].email;
      } else {
        tempRoom = tempOnline[i].email + '*' + this.tokenService.getuser().email;
      }
      var roomIndex = roomList.findIndex((obj) => {
        return obj.rid == tempRoom;
      });
      //console.log('roominde:' + roomIndex);
      if (roomIndex == -1) {
        roomList.push({ displayname: tempOnline[i].displayname, rid: tempRoom }); // รูปแบบ rid : email*email
      }
    }
    //console.log(roomList);
    return roomList;
  }

  findRoom(displayname) {
    //console.log('finroom');
    var online = this.getonlineuser();
    var indexRoom = this.createRoom().findIndex((obj) => {
      // spectify room
      return obj.displayname == displayname;
    });

    return this.createRoom()[indexRoom].rid;
  }

  joinRoom(roomid) {
    //this.socket.emit('testSendUser', email);
    this.socket.emit('joinRoom', roomid);
  }

  storageMsgbox(data) {
    // เป็นการเก็บ msgbox ไว้ในเครื่อง
    var roomid = data.rid;
    var newMsg = data.msg;
    var storemsg: string[] = [];
    storemsg.push(newMsg);
    var roomAndMsg = { rid: roomid, msg: storemsg };
    var indexRoomID = this.roomMapMsg.findIndex((obj) => {
      return obj.rid == roomid; // หา roomid นี้มี msg ไหม เพื่อเก็บ
    });
    if (indexRoomID == -1) {
      this.roomMapMsg.push(roomAndMsg);
    } else {
      this.roomMapMsg[indexRoomID].msg.push(storemsg); // ถ้า roomandmsg ไม่มี roomid จะทำการ push ทั้งหมด
    }
  }

  loadingMsg(roomid) {
    var tempMsgStore: string[] = [];
    var index = this.roomMapMsg.findIndex((obj) => {
      return obj.rid == roomid; // หา roomid นี้มี msg ไหม เพื่อใช้งาน
    });
    if (index !== -1) {
      tempMsgStore = this.roomMapMsg[index].msg;
    }
    return tempMsgStore;
  }

  sendMsg(data) {
    this.socket.emit('sendMsg', data);
  }

  testReceived() {
    // เพื่อให้ใช้ sub ของ data ได้
    let observable = new Observable<string>((observer) => {
      this.socket.on('sendMsg', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  async getMsg() {
    this.socket.on('sendMsg', (data) => {
      this.storageMsgbox(data);
      console.log(data.msg);
    });
  }

  userDisconnect() {
    if (this.socket) {
      // check connect sokcet ?
      this.socket.emit('userdisconnect', this.tokenService.getuser().email);
      this.socket.disconnect();
    }
  }
}
