import { TestBed } from '@angular/core/testing';

import { AuthenserviceService } from './authenservice.service';

describe('AuthenserviceService', () => {
  let service: AuthenserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthenserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
