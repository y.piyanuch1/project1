import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherserviceService {
  constructor(private http: HttpClient) {}
  getWeather() {
    return this.http.get(
      'https://api.openweathermap.org/data/2.5/weather?q=bangkok&appid=4fee0d66b0f67654aaa5ba0c643fa1cf'
    );
  }
}
