import { Component, OnInit } from '@angular/core';
import { Socket } from 'socket.io-client';
import { TokenserviceService } from 'src/app/services/tokenservice.service';
import { ChatserviceService } from '../../services/chatservice.service';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  textmsg: string;
  displayname: string = ''; // myname
  onlineuser: any = [];
  onlineNumber: number;
  name: string = 'CHATBOX'; // display on chatbox
  Warning: string = 'Please Select User =>';
  chatmsg: string[] = [];
  constructor(private chatService: ChatserviceService, private tokenservice: TokenserviceService) {
    this.chatService.testReceived().subscribe((data) => {
      this.chatService.storageMsgbox(data);
      var roomid = this.chatService.findRoom(this.name.trim());
      this.chatmsg = this.chatService.loadingMsg(roomid);
    });
  }

  ngOnInit(): void {
    this.displayname = this.tokenservice.getuser().displayname;
    this.chatService.setupconnect();
    /*
    this.chatService.getMsg();
    var roomid = this.chatService.findRoom(this.name.trim());
    console.log('cons');
    console.log(roomid);
    this.chatmsg = this.chatService.loadingMsg(roomid);*/
  }

  getonlineUser() {
    var online = this.chatService.getonlineuser();
    return online;
  }

  clickUser(event) {
    // select user
    this.Warning = '';
    this.name = event.target.innerHTML;
    var roomid = this.chatService.findRoom(this.name.trim());
    console.log(roomid);
    this.chatService.joinRoom(roomid);
    this.chatService.loadingMsg(roomid);
  }

  clickSend() {
    var sendmsgA = this.tokenservice.getuser().displayname + ': ' + this.textmsg;
    //console.log('This nam:');
    //console.log(this.name.trim());
    var roomid = this.chatService.findRoom(this.name.trim());
    this.chatService.sendMsg({ rid: roomid, msg: sendmsgA });
    this.textmsg = '';
  }
}
