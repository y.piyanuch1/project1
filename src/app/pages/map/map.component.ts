import { Component, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { WeatherserviceService } from 'src/app/services/weatherservice.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  map: any;
  maptoken = 'pk.eyJ1IjoicGl5YW51Y2gxIiwiYSI6ImNsMDVndjY4YzF1ZTYzaXBtNWI1djBhMHcifQ.v4L1bv9wosqhAvjj1bPhdg';
  style = 'mapbox://styles/mapbox/streets-v11';
  weatherL: any = []; //get lat+long (array)
  marker: any;
  constructor(private weatherapi: WeatherserviceService) {}

  ngOnInit(): void {
    this.weatherapi.getWeather().subscribe((response: any) => {
      this.weatherL = response;
      this.map = new mapboxgl.Map({
        container: 'map',
        accessToken: this.maptoken,
        style: this.style,
        zoom: 11,
        center: [this.weatherL.coord.lon, this.weatherL.coord.lat]
      });
      this.marker = new mapboxgl.Marker({ color: '#FFC72C' })
        .setLngLat([this.weatherL.coord.lon, this.weatherL.coord.lat])
        .addTo(this.map);
    });
  }
}
