import { Component, OnInit } from '@angular/core';
import { WeatherserviceService } from 'src/app/services/weatherservice.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  weather: any = [];

  constructor(private weatherService: WeatherserviceService) {}

  ngOnInit(): void {
    this.weatherService.getWeather().subscribe((response: any) => {
      this.weather = response.results;
      console.log(this.weather);
    });
  }
}
